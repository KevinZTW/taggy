package service

import (
	"Taggy/internal/pkg/config"
	"Taggy/internal/pkg/repository"
)

type Client struct{
	RepositoryClient *repository.Client
}

func NewClient(conf *config.Config) *Client{
	return &Client{
		RepositoryClient: repository.NewClient(conf.DatabaseURL, conf.Database),
	}
}