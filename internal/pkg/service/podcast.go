package service

import "Taggy/internal/pkg/repository/podcast"

func (c *Client) GetPodcastList()[]*podcast.Data{
	return c.RepositoryClient.PodcastDataCollection.ListData()
}