package repository

import (
	"Taggy/internal/pkg/repository/podcast"
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)
const (
	PodcastDataCollection = "PodcastDataCollection"
)
type Client struct{
	Client *mongo.Client
	Database string
	PodcastDataCollection *podcast.DataCollection
}

func NewClient(url, database string) *Client{
	if client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(url)); err != nil {
		panic(err.Error())
	} else {
		db := client.Database(database)
		return &Client{
			Client : client,
			Database: database,
			PodcastDataCollection: podcast.NewDataCollection(db.Collection(PodcastDataCollection)),
		}
	}

}