package podcast

import (
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type Data struct{
	Name string `bson:"name" `
	Description string `bson:"description"`
	RSSURL string `bson:"RSSURL"`
}

type DataCollection struct{
	collection *mongo.Collection
}

func NewDataCollection(collection *mongo.Collection) *DataCollection{
	return &DataCollection{
		collection: collection,
	}
}

func (c *DataCollection) ListData() []*Data{
	filter := bson.M{}
	fmt.Println(c.collection)
	if cur, err := c.collection.Find(nil, &filter);err!=nil{
		fmt.Println(err)
		return nil
	}else {
		var datalist [] *Data
		cur.All(nil, &datalist)
		return datalist
	}
}
