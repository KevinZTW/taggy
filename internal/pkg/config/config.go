package config

import (
	"fmt"
	"github.com/joho/godotenv"
	"os"
)

type Config struct{
	Database    string
	DatabaseURL string
}

func LoadEnvConfig(path string) *Config{
	if err := godotenv.Load(path); err != nil {
		fmt.Println("not used .env")
	}
	return &Config{
		Database:    os.Getenv("DATABASE"),
		DatabaseURL: os.Getenv("DATABASE_URL"),
	}
}
