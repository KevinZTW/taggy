package route

import (
	"Taggy/internal/pkg/service"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Host(r *gin.Engine, client *service.Client){
	r.GET("./", func(c *gin.Context){
		c.String(http.StatusOK, "hello world")
	})
}
