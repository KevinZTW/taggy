package route

import (
	"Taggy/internal/pkg/service"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Podcast(r *gin.Engine, client *service.Client){
	r.GET("/podcast", func(c *gin.Context){
		podcasts := client.GetPodcastList()
		c.JSON(http.StatusOK, podcasts)
	})
}
