package route

import (
	"Taggy/internal/pkg/service"
	"github.com/gin-gonic/gin"
)

func SetUp(r *gin.Engine, client *service.Client){
	Host(r, client)
	Podcast(r, client)
}