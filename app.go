package main

import (
	"Taggy/internal/pkg/config"
	"Taggy/internal/pkg/route"
	"Taggy/internal/pkg/service"
	"fmt"
	"github.com/gin-gonic/gin"
)

func main(){
	//load config
	conf := config.LoadEnvConfig(".env")
	fmt.Println("Use Database :", conf.Database)
	fmt.Println("Use Database Url:", conf.DatabaseURL)

	//init service & repository
	Client := service.NewClient(conf)

	r := gin.Default()
	route.SetUp(r, Client)

	r.Run(":5050")
}
